
class Laptop implements Systemoperasi, Merek, spesifikasi{
	String nama = "Asus"; 
	String asal = "Jepang";
	String os = "Windows 10";
	String ram = "12 GB";	
	
	public void spek(){
		System.out.println("\nSpesifikasi laptop ini adalah : " );
		System.out.println("Processor");
		System.out.println("Intel® Core™ i9-10980HK Processor 2.4 GHz (16M Cache, up to 5.3 GHz, 8 cores)");
		System.out.println("Ram " + ram);		
	}
	public void jenis(){
		System.out.println("Yang anda pilih adalah Laptop : " + nama);
		System.out.println("Asal mereknya dari : " + asal);
	}
	public void jenisSystem(){
		System.out.println("Dan juga memiliki os : " + os + "\n");
	}
}